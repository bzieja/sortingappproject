package pl.bzieja;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * The main test class. Runs test suites for testing the Sorting App {@link SortingApp}
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({CornerCasesSortTest.class, CommonCasesSortTest.class})
public class SortingAppTest {
}