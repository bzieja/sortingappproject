package pl.bzieja;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

/**
 * Parameterized test class which contains common cases for sorting task.
 */
@RunWith(Parameterized.class)
public class CommonCasesSortTest {

    private SortingApp sortingApp = new SortingApp();
    int[] array;
    int[] sortedArray;

    public CommonCasesSortTest(int[] array, int[] sortedArray) {
        this.array = array;
        this.sortedArray = sortedArray;
    }

    @Test
    public void should_sortArray_when_sortMethodInvoked() {
        Assert.assertTrue(true);
    }

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][] {
                {new int[]{4, 2, 1, 3, 5}, new int[]{1, 2, 3, 4, 5}},
                {new int[]{-99, -100, -123, -90, 0}, new int[]{-123, -100, -99, -90, 0}},
                {new int[]{1, -1, 2, -2, 3}, new int[]{-2, -1, 1, 2, 3}},
                {new int[]{-2, -4, -6, -8, -10}, new int[]{-10, -8, -6, -4, -2}},
                {new int[]{1000, -10000, 20000}, new int[]{-10000, 1000, 20000}},
        });
    }

}
