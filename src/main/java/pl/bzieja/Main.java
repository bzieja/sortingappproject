package pl.bzieja;

import java.util.Arrays;
import java.util.Scanner;

public class Main {

    /**
     * The main method creates an instance of the sorting app {@link SortingApp}.
     * It reads integers from the command line (or from the standard input if the command line is empty) and prints them sorted to the standard output.
     * @param args Parameters passed to the main method from the command line. They are ignored in this program.
     */
    public static void main(String[] args) {
        SortingApp sortingApp = new SortingApp();
        int[] arrayToSort;

        if (args.length == 0) {
            arrayToSort = Arrays.stream(new Scanner(System.in).nextLine().trim().replaceAll(" +", " ").split(" ")).mapToInt(Integer::valueOf).toArray();
        } else {
            arrayToSort = Arrays.stream(args).mapToInt(Integer::valueOf).toArray();
        }

        sortingApp.sort(arrayToSort);
        Arrays.stream(arrayToSort).forEach(number -> System.out.print(number + " "));
    }

}
