package pl.bzieja;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.Random;

/**
 * Test class which contains corner cases for sorting task.
 */
@RunWith(JUnit4.class)
public class CornerCasesSortTest {

    SortingApp sortingApp = new SortingApp();

    @Test(expected = IllegalArgumentException.class)
    public void testNullCase(){
        int[] array = null;

        sortingApp.sort(array);

        Assert.assertNull(array);
    }

    @Test
    public void testEmptyCase(){
        int[] array = new int[]{};

        sortingApp.sort(array);

        Assert.assertArrayEquals(new int[]{}, array);
    }

    @Test
    public void testSingleElementArrayCase() {
        int singleInt = new Random().nextInt();
        int[] array = new int[]{singleInt};

        sortingApp.sort(array);

        Assert.assertArrayEquals(new int[]{singleInt}, array);
    }

    @Test
    public void testTenElementsArrayCase() {
        int[] array = new int[]{3, 2, 1, 6, 5, 4, 10, 9, 8, 7};

        sortingApp.sort(array);

        Assert.assertArrayEquals(new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}, array);
    }

    @Test
    public void testMoreThanTenElementsArrayCase() {
        int[] array = new int[]{3, 2, 1, 6, 5, 4, 10, 9, 8, 7, 15, 14, 13, 12, 11};

        sortingApp.sort(array);

        Assert.assertArrayEquals(new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15}, array);

    }

    @Test
    public void testSortedArraysCase() {
        int[] array = new int[] {-1, 1, 2, 3, 4, 5};

        sortingApp.sort(array);

        Assert.assertArrayEquals(new int[] {-1, 1, 2, 3, 4, 5}, array);
    }

    @Test
    public void testArrayWithTheIntBoundaryValues() {
        int[] array = new int[] {Integer.MAX_VALUE, Integer.MIN_VALUE, Integer.MIN_VALUE, Integer.MAX_VALUE};

        sortingApp.sort(array);

        Assert.assertArrayEquals(new int[] {Integer.MIN_VALUE, Integer.MIN_VALUE, Integer.MAX_VALUE, Integer.MAX_VALUE}, array);
    }

    @Test
    public void testArrayWithTheSameValues() {
        int[] array = new int[] {-1, -1, -1, -1, -1, -1};

        sortingApp.sort(array);

        Assert.assertArrayEquals(new int[] {-1, -1, -1, -1, -1, -1}, array);
    }
}
