package pl.bzieja;

/**
 * This class sorts the array of ints passed to the sort method.
 * Uses bubble sort algorithm.
 */
public class SortingApp {

    /**
     * This method sorts the array of ints.
     * It throws the IllegalArgumentException when the passed array is null.
     * @param array An array of integers which the algorithm should sort.
     */
    public void sort(int[] array) {

        if (array == null) {
            throw new IllegalArgumentException();
        }

        boolean hasBeenChanged;

        do {
            hasBeenChanged = false;

            for (int i = 0; i < array.length - 1; i++) {
                if (array[i] > array[i + 1]) {
                    int tmp = array[i];
                    array[i] = array[i + 1];
                    array[i + 1] = tmp;
                    hasBeenChanged = true;
                }
            }

        } while (hasBeenChanged);
    }
}
